package executor;

import system_boundary.Command;
import system_boundary.CommandCreator;
import executor.exceptions.InvalidCommandException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class AvailableCommands {

    protected Map<String, Map<String, Command>> commandMap;

    public AvailableCommands() {
        commandMap = new HashMap<>();
    }

    public void initialize(List<CommandCreator> commandCreatorList) {
        commandCreatorList.forEach(creator -> {
            commandMap.put(creator.getPeripheralName(), creator.makeCommands());
        });
    }

    public Optional<Command> readCommand(String peripheral, String action) {
        if(!commandMap.containsKey(peripheral)) {
            throw new InvalidCommandException("Peripheral not found " + peripheral);
        }
        if(!commandMap.get(peripheral).containsKey(action)) {
            throw  new InvalidCommandException("Action not found " + action);
        }
        return Optional.ofNullable(commandMap.get(peripheral).get(action));
    }
}
