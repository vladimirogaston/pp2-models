package executor;

import system_boundary.CommandCreator;
import executor.exceptions.InvalidCommandException;

import java.util.List;

public class CommandExecutor extends Executor {

    public CommandExecutor(List<CommandCreator> commandsCreators) {
        super(commandsCreators);
    }

    protected void execute(InputCommand inputCommand) {
        final String peripheral = inputCommand.getPeripheral();
        final String action = inputCommand.getAction();
        availableCommands.readCommand(peripheral, action).ifPresent(command -> {
            try{
                Integer times = Integer.parseInt(inputCommand.getTimes());
                for(int index = 0; index < times; index++) {
                    command.execute();
                }
            }catch (NumberFormatException e) {
                throw new InvalidCommandException("Invalid parameter: " + inputCommand.getTimes());
            }
        });
    }
}