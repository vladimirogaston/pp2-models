package executor;

import executor.exceptions.InvalidCommandException;
import system_boundary.CommandCreator;

import java.util.List;

public abstract class Executor {

    protected AvailableCommands availableCommands;

    public Executor(List<CommandCreator> commandsCreators) {
        availableCommands = new AvailableCommands();
        availableCommands.initialize(commandsCreators);
    }

    protected abstract void execute(InputCommand input) throws InvalidCommandException;

    public void execute(String input) {
        List<InputCommand> inputCommandList = new InputProcessor(input).makeExecutableCommands();
        inputCommandList.forEach(ic -> execute(ic));
    }
}
