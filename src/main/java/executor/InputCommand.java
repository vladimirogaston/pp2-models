package executor;

public class InputCommand {
    public String peripheral;
    public String action;
    public String times;

    public InputCommand() {
        peripheral = "";
        action = "";
        times = "";
    }

    public InputCommand peripheral(String peripheral) {
        this.peripheral = peripheral;
        return this;
    }

    public InputCommand action(String action) {
        this.action = action;
        return this;
    }

    public InputCommand times(String times) {
        this.times = times;
        return this;
    }

    public String getPeripheral() {
        return peripheral;
    }

    public String getAction() {
        return action;
    }

    public String getTimes() {
        return times;
    }

    @Override
    public String toString() {
        return "InputCommand{" +
                "peripheral='" + peripheral + '\'' +
                ", action='" + action + '\'' +
                ", times='" + times + '\'' +
                '}';
    }
}
