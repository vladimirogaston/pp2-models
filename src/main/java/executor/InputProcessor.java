package executor;

import executor.exceptions.InvalidCommandException;

import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class InputProcessor {

    private static final String INVALID_INPUT = "Invalid input on: ";
    private static final String COMMAND_DELIMITER = "^";
    private static final String PERIPHERAL_DELIMITER = ":";
    private static final String PARAM_DELIMITER = ">";
    private String entry;

    public InputProcessor(String input) {
        this.entry = input;
    }

    public List<InputCommand> makeExecutableCommands() {
        List<String> commands = tokenizer(entry.trim(), COMMAND_DELIMITER);
        List<InputCommand> commands1 = commands.stream().map(str->{
            return toInputCommand(str);
        }).collect(Collectors.toList());
        return commands1;
    }

    private List<String> tokenizer(String in, String del) {
        List<String> tokens = Collections.list(new StringTokenizer(in, del)).stream().map(tok -> (String)tok).collect(Collectors.toList());
        return tokens;
    }

    // turnOn:AirConditioner>10
    private InputCommand toInputCommand(String in) {
        InputCommand ic = new InputCommand();
        if(in.contains(PARAM_DELIMITER)) {
            ic.times(getParameter(in));
            List<String> tokens = tokenizer(tokenizer(in, PARAM_DELIMITER).get(0), PERIPHERAL_DELIMITER);
            ic.action(getAction(tokens));
            ic.peripheral(getPeripheral(tokens));
        } else {
            List<String> tokens = tokenizer(in, PERIPHERAL_DELIMITER);
            ic.action(getAction(tokens));
            ic.peripheral(getPeripheral(tokens));
        }
        return ic;
    }

    private String getParameter(String in) {
        String param = "";
        if(in.contains(PARAM_DELIMITER)){
            List<String> tokens = tokenizer(in, PARAM_DELIMITER);
            if(tokens.size() != 2) throw new InvalidCommandException(INVALID_INPUT + in);
            param = tokens.get(1);
        }
        return param;
    }

    private String getAction(List<String> tokens) {
        validateInput(tokens);
        return tokens.get(0);
    }

    private String getPeripheral(List<String> tokens) {
        validateInput(tokens);
        return tokens.get(1);
    }

    private void validateInput(List<String> in) {
        if(in.size() != 2) throw new InvalidCommandException(INVALID_INPUT + in);
    }
}
