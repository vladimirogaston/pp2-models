package system_boundary;

import executor.exceptions.ExecutionException;

public interface Command {

    void execute() throws ExecutionException;
}
