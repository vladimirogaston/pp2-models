package system_boundary;

public interface Observer {

    void update(Object object);
}
